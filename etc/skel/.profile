export AMD_VULKAN_ICD=RADV
export BROWSER=firefox
export EDITOR=/usr/bin/nano
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
export MAIL=thunderbird
export TERM=xterm
export VISUAL=kate
